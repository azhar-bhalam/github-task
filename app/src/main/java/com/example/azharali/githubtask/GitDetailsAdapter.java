package com.example.azharali.githubtask;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Azhar Ali Bhalam on 25-04-2018.
 */

public class GitDetailsAdapter extends BaseAdapter {

    private ViewHolder viewHolder;
    private LayoutInflater mInflater;

    private Context context;
    private ArrayList<GitDetailsModel> list;

    public GitDetailsAdapter(Context context, ArrayList<GitDetailsModel> list) {
        this.context = context;
        this.list = list;
        mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_git_details, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.tvName.setText(list.get(i).getName());
        viewHolder.tvCode.setText("Code: " + list.get(i).getCode());
        viewHolder.tvMessage.setText("Commit: " + list.get(i).getMessage());

        Picasso.get().load(list.get(i).getAvatar_url()).placeholder(R.mipmap.ic_launcher_round).into(viewHolder.ivAvatar);

        return convertView;
    }

    static class ViewHolder {

        TextView tvName, tvCode, tvMessage;
        CircleImageView ivAvatar;

        ViewHolder(View v) {
            tvName = (TextView) v.findViewById(R.id.tvName);
            tvCode = (TextView) v.findViewById(R.id.tvCode);
            tvMessage = (TextView) v.findViewById(R.id.tvMessage);
            ivAvatar = (CircleImageView) v.findViewById(R.id.ivAvatar);
        }

    }
}
