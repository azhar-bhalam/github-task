package com.example.azharali.githubtask;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog dialog;
    private ListView lvGithub;

    private ArrayList<GitDetailsModel> gitDetailsList;
    private GitDetailsModel gitDetailsModel;
    private GitDetailsAdapter gitDetailsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Fetching details ...");
        lvGithub = (ListView) findViewById(R.id.lvGithub);
        gitDetailsList = new ArrayList<>();

        if (savedInstanceState == null) {
            fetchGitData();
        }

    }

    private void fetchGitData() {
        dialog.show();

        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);

        Log.i("Details URL", "https://api.github.com/repos/rails/rails/commits");
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, "https://api.github.com/repos/rails/rails/commits", null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.dismiss();
                        try {
                            gitDetailsList.clear();

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = response.getJSONObject(i);
                                String sha = obj.getString("sha");
                                String commit = obj.getString("commit");
                                JSONObject obj1 = new JSONObject(commit);
                                String message = obj1.getString("message");
                                String author = obj.getString("author");
                                JSONObject obj2 = new JSONObject(author);
                                String login = obj2.getString("login");
                                String avatar_url = obj2.getString("avatar_url");
                                gitDetailsModel = new GitDetailsModel(login, sha, message, avatar_url);
                                gitDetailsList.add(gitDetailsModel);
                            }

                            if (gitDetailsList.size() != 0) {
                                gitDetailsAdapter = new GitDetailsAdapter(MainActivity.this, gitDetailsList);
                                lvGithub.setAdapter(gitDetailsAdapter);
                                gitDetailsAdapter.notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        // display response
                        Log.d("Details Response", response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Log.d("Error.Response", error.toString());
                    }
                }
        );
        int socketTimeout = 600000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        getRequest.setRetryPolicy(policy);
        queue.add(getRequest);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("gitDetails", gitDetailsList);
        Log.i("saving", "called");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            gitDetailsList = savedInstanceState.getParcelableArrayList("gitDetails");
            gitDetailsAdapter = new GitDetailsAdapter(MainActivity.this, gitDetailsList);
            lvGithub.setAdapter(gitDetailsAdapter);
            gitDetailsAdapter.notifyDataSetChanged();
            Log.i("restore", "called");
        }
    }
}
