package com.example.azharali.githubtask;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Azhar ALi Bhalam on 25-04-2018.
 */

public class GitDetailsModel implements Parcelable {

    String name, code, message, avatar_url;

    public GitDetailsModel(String name, String code, String message, String avatar_url) {
        this.name = name;
        this.code = code;
        this.message = message;
        this.avatar_url = avatar_url;
    }

    protected GitDetailsModel(Parcel in) {
        name = in.readString();
        code = in.readString();
        message = in.readString();
        avatar_url = in.readString();
    }

    public static final Creator<GitDetailsModel> CREATOR = new Creator<GitDetailsModel>() {
        @Override
        public GitDetailsModel createFromParcel(Parcel in) {
            return new GitDetailsModel(in);
        }

        @Override
        public GitDetailsModel[] newArray(int size) {
            return new GitDetailsModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(code);
        parcel.writeString(message);
        parcel.writeString(avatar_url);
    }
}
